
// XTileScriptingDlg.h : header file
//

#pragma once
#include "xdevice.h"

#include "xsystem.h"
#include "xcommand.h"
#include "xgig_factory.h"
#include "xacquisition.h"
#include "xframe_transfer.h"
#include "xdisplay.h"
#include "xoff_correct.h"
#include "xanalyze.h"
#include "xmulti_transfer.h"
#include "xarea_scan_correct.h"

#include "afxwin.h"
#ifdef _WIN64
#pragma comment(lib, "..\\..\\lib\\64\\XLibDllXTile.lib")
#else
#pragma comment(lib, "..\\lib\\32\\XLibDllXTile.lib")
#endif


#define WM_EVENT		 WM_USER+1
#define WM_ERROR		 WM_USER+2
#define WM_FRAME_READY	 WM_USER+3
#define WM_ANALYSIS		 WM_USER+4

#define DEVICE_NUM 5
#define NUM_GAIN_SETTINGS 8
#define MAX_NUM_SCRIPT_RUNS 50
#define KV_SETTING 0
#define MA_SETTING 1
#define FRAMES 2
#define BITS 3
#define INT_TIME 4
#define DMS 5


// CXTileScriptingDlg dialog
class CXTileScriptingDlg : public CDialogEx
{
// Construction
public:
	CXTileScriptingDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_XTILESCRIPTING_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	void CXTileScriptingDlg::OnClose();


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	void GetSN();
	void RegisterSink();
	LRESULT	OnFrameReadyMsg(WPARAM wParam, LPARAM lParam);

	int			_color_mode;
	int			_dev_num;
	CString		_mac_str;
	CString		_ip_str;
	UINT		_cmd_port;
	UINT		_img_port;
	bool		_is_open;
	UINT		_grid_x;
	UINT		_grid_y;
	int			_scan_fun;
	UINT		_line_number;
	UINT		_tile_x;
	UINT		_tile_y;
	char*		tempbuf;	//pointer to test memory cache
	uint32_t	bufsize;   //alloc memory size
	uint32_t	bufuse;    //memory use size
	CString		_card_nums;
	CEdit		m_Pathname;
	CEdit		m_Filename;
	CEdit		m_OutputFile;
	CEdit		m_connected;
	BOOL		_bFocusFilename;
	int			_bRunScript;    
	std::string _SavePath_;
	CString		_ScriptFile;
	UINT		_total_frames;
	int			_cur_test_run;
	int			_cur_gain;


	CIPAddressCtrl		_ip_control;

	XSystem			_xsystem;
	XCommand		_xcommand[DEVICE_NUM];
	XAcquisition	_xacquisition[DEVICE_NUM];
	XFrameTransfer	_xtransfer[DEVICE_NUM];
	XDisplay		_xdisplay;
	XOffCorrect		_xoff_correct[DEVICE_NUM];
	XGigFactory		_xfactory;
	XAnalyze*		_xanalyze;
	XMultiTransfer	_xmulti_transfer;
	XImage*			_cur_image_;    
	XAreaScanCorrect _xarea_correct;

	std::string		_kV;
	std::string		_mA;
	std::string		_frames;
	std::string		_bits;
	std::string		_intTime;
	std::string		_gain[8];
	int				_num_gain_settings;
	CString			_outputFileName;
	UINT			_int_time;


	int		_script_params[MAX_NUM_SCRIPT_RUNS][6];
	int		_script_gains[MAX_NUM_SCRIPT_RUNS][8];


	uint32_t		_frame_counter; //total frame counter,reset when new acquisition
	uint32_t		_line_lost_num; //Line lose counter , reset when new acquisition
	uint32_t		_cur_dev;       //the Device seq id

	int				_op_mode;
	BOOL			_en_line_trigger;
	BOOL			_connected;

	int					_bDisplay;      //Is need to Display 
	int					_bSaveImage;    //Is need to Save File
	int					_SaveInterval;	//saving file interval count
	int					_SaveCount;	    //current saved image count
	int					_SaveFlag;      //middle param for count Interval count
	int					_bFileSaved;    // -for judge file is save?
	CString				_SavePrefix;        //save filename
	CString				_SaveSuffix;
	CString				_SavePath;  //save path

	CWnd* GetDispWnd();

	BOOL CXTileScriptingDlg::ParseJsonFile();

	afx_msg void OnBnClickedConnect();
	afx_msg void OnIpnFieldchangedIpaddressGcuIp(NMHDR* pNMHDR, LRESULT* pResult);
	//afx_msg void OnSetfocusEditFilename();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedReadScriptFile();
	afx_msg void OnBnClickedRunScript();
	afx_msg void OnBnClickedConvertTemps();
	void SaveImage(CString file_name);
	uint32_t hex2int(char* hex);
	void WriteAuxFile(CString* gain);
	LRESULT OnErrorMsg(WPARAM wParam, LPARAM lParam);
	LRESULT OnEventMsg(WPARAM wParam, LPARAM lParam);
	int ConvertGainString(std::string* gain);
	CString ConvertGainInt(int gain);
	void SetOutputFileName(int test_run_index);
	void OnBnClickedBtnCap(int Start);


};
