/*
  Copyright (c) 2014, Detection Technology Inc.
  All right reserved.

  Author: Zhang Xu
 */

#ifndef XROTATE_CORRECT_H
#define XROTATE_CORRECT_H

#include "ixcorrect.h"

class XDLL_EXPORT XRotateCorrect : public IXCorrect
{
public:
     XRotateCorrect();
     ~XRotateCorrect();

     bool DoCorrect(XImage* image_);
     XImage* GetRotImage();
private:

     XRotateCorrect(const XRotateCorrect&);
     XRotateCorrect& operator = (const XRotateCorrect&);

     XImage _rot_image;
};

#endif //XROTATE_CORRECT_H
