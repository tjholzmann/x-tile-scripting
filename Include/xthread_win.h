/*
Copyright (c) 2014, Detection Technology Inc.
All rights reserved.

This file defines some thread related classes.

Author: Zhang Xu, 2014-3-5
*/

#ifndef XTHREAD_WIN_H
#define XTHREAD_WIN_H

#include <windows.h>
#include <stdint.h>
#include <process.h>

/*
This class simplely wraps the critical_section object.
It provides the basic locking function.
*/
class XDLL_EXPORT XLock
{
public:
	XLock()
	{
		InitializeCriticalSection(&_critical_sect);
	};

	~XLock()
	{
		DeleteCriticalSection(&_critical_sect);
	};

	void Lock()
	{
		EnterCriticalSection(&_critical_sect);
	};

	void Unlock()
	{
		LeaveCriticalSection(&_critical_sect);
	};

private:
	CRITICAL_SECTION	_critical_sect;
};

class XDLL_EXPORT XEvent
{
public:
	XEvent()
	{
		_event_obj = CreateEvent(NULL, TRUE, FALSE, NULL);
	}
	~XEvent()
	{
		CloseHandle(_event_obj);
	}

	void Set()
	{
		SetEvent(_event_obj);
	}
	void WaitTime(int32_t millisecond)
	{
		ResetEvent(_event_obj);
		WaitForSingleObject(_event_obj, millisecond);
	}
	void Wait()
	{
		ResetEvent(_event_obj);
		WaitForSingleObject(_event_obj, INFINITE);
	}
private:
	HANDLE _event_obj;
};

class XDLL_EXPORT XMultiEvents
{
public:
	XMultiEvents(uint32_t event_num)
		: _event_num(event_num)
	{
		if(0 == event_num)
			_event_objs_ = NULL;
		else
		{
			_event_objs_ = new HANDLE[event_num];
			for(uint32_t i=0; i<event_num; i++)
				_event_objs_[i] = CreateEvent(NULL,TRUE,FALSE,NULL);
		}
	}
	~XMultiEvents()
	{
		if(_event_objs_)
		{
		for(uint32_t i=0; i<_event_num; i++)
			CloseHandle(_event_objs_[i]);
		delete[] _event_objs_;
		}
	}

	void Set(uint32_t index)
	{
		if(_event_objs_)
		{
		if(index < _event_num)
			SetEvent(_event_objs_[index]);
		}
	}
	void WaitTime(int32_t millisecond)
	{
		if(_event_objs_)
		{
		for(uint32_t i = 0; i<_event_num;i++)
			ResetEvent(_event_objs_[i]);
		WaitForMultipleObjects(_event_num, _event_objs_, TRUE, millisecond);
		
		}
	}
	void Wait()
	{
		if(_event_objs_)
		{
		for(uint32_t i = 0; i<_event_num;i++)
			ResetEvent(_event_objs_[i]);
		WaitForMultipleObjects(_event_num, _event_objs_, TRUE, INFINITE);
		
		}	
	}

private:
	HANDLE* _event_objs_;
	uint32_t _event_num;
};
/*
This calss wraps basic thread funcitons. Use thread function and arguments
as parameters when claiming.
If circulation in thread function needs to checkstop flag, do like this
"while(!thread_obj.IsStopped())". 
*/

#define XTERMINATION_WAIT_INTERVAL	3000		//Force to terminate the thread after interval

typedef unsigned * PBEGINTHREADEX_THREADID;
typedef unsigned (WINAPI *PBEGINTHREADEX_THREADFUNC) (LPVOID lpThreadParameter);

class XDLL_EXPORT XThread
{
public:
	typedef uint32_t (__stdcall *ThreadFunc) (void *);

	XThread(ThreadFunc func_, void* arg_)
		:_thread_func_(func_)
		,_thread_arg_(arg_)
		,_thread_handle(NULL)
		,_thread_id(0)
	{
		_stop_event = CreateEvent(NULL, TRUE, FALSE, NULL);
	};

	~XThread()
	{
		Stop();
		CloseHandle(_stop_event);
	};

	bool Start(bool high_priority=0)
	{
		//Already started
		//if(_thread_handle)
		//	return 1;

		//If fail, return NULL
		_thread_handle = (HANDLE) _beginthreadex(NULL, 0, 
												(PBEGINTHREADEX_THREADFUNC)_thread_func_,
												_thread_arg_, 0, &_thread_id);
	    //Make stop event object active
		if(_thread_handle)
		{
			ResetEvent(_stop_event);
			if(high_priority)
			{
				SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
				SetThreadPriority(_thread_handle, THREAD_PRIORITY_TIME_CRITICAL);
			}

		}
		else
			return 0;
		return  1;
	}

	/*
	Stop thread with stop event. If it doesn't exit within XTERMINATION_WAIT_INTERVAL seconds,
	then force it to terminate. If it is forced to terminate, it'll return 0 and can't start 
	again correctly.
	*/
	bool Stop()
	{
		bool ret = 1;
		if(_thread_handle)
		{
			if(IsActive())
			{
				SetEvent(_stop_event);
				//Wait "XTERMINAL_WAIT_INTERVAL" seconds for thread to terminate
				if(WaitForSingleObject(_thread_handle, XTERMINATION_WAIT_INTERVAL) != WAIT_OBJECT_0)
				{
					if(IsActive())
					{
						//Force to terminate it
						TerminateThread(_thread_handle, int32_t(-1));
						//This yield allows the sheduler to do additional clean up
						Sleep(0);
						ret = 0;
					}
				}
			}
			//Clean up
			CloseHandle(_thread_handle);
			_thread_handle = NULL;
			_thread_id = 0;
		}
		return ret;
	}
	//Useless in windows
	void Exit()
	{
	
	};
	
	bool IsStopped()
	{
		uint32_t result;
		result = WaitForSingleObject(_stop_event,0);
		return (WAIT_OBJECT_0 == result); 
	}
	
	uint32_t GetThreadId()
	{
		return GetCurrentThreadId();
	}

private:	
	XThread(const XThread&);
	XThread& operator = (const XThread&);

	bool IsActive()
	{
		uint32_t exit_code = -1;
		GetExitCodeThread(_thread_handle, (LPDWORD)&exit_code);
		return (STILL_ACTIVE == exit_code);
	}
	
	HANDLE _thread_handle;
	HANDLE _stop_event;
	uint32_t _thread_id;

	ThreadFunc	_thread_func_;	//Thread function pointer
	void*		_thread_arg_;	//Thread fucntion argument 

};

#endif //XTHREAD_WIN_H
