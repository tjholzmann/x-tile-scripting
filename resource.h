//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by XTileScripting.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_XTILESCRIPTING_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_CONNECT_DETECTORS           1000
#define IDC_GCU_MAC                     1005
#define IDC_IPADDRESS_GCU_IP            1006
#define IDC_EDIT_GCU_CMD_PORT           1007
#define IDC_EDIT_GCU_IMG_PORT           1008
#define IDC_EDIT_FILENAME               1009
#define IDC_READ_SCRIPT_NAME            1011
#define IDC_READ_SCRIPT_FILE            1012
#define IDC_TEST_RUN_PARAMS             1013
#define IDC_RUN_SCRIPT                  1014
#define IDC_CONNECTED                   1015
#define IDC_DISPLAY_WND                 1016
#define IDC_BUTTON1                     1017
#define IDC_CONVERT_TEMPS               1017
#define IDC_EDIT_TILE_Y                 1018
#define IDC_EDIT2                       1019
#define IDC_EDIT_TILE_X                 1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
