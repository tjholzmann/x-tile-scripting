
// XTileScriptingDlg.cpp : implementation file
//

#include "pch.h"
#include "XTileScripting.h"
#include "XTileScriptingDlg.h"
#include "xtif_format.h"
#include "afxdialogex.h"
#include <json.h>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}


void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


CXTileScriptingDlg* demo_dlg_;


// CXTileScriptingDlg dialog

CXTileScriptingDlg::CXTileScriptingDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_XTILESCRIPTING_DIALOG, pParent)
	, _dev_num(0)
	, _scan_fun(0)
	, _tile_x(1)
	, _tile_y(4)
	, _ScriptFile('\0')
	, _op_mode(0)
	, _color_mode(0)
	, _connected(FALSE)
	, _frame_counter(0)
	, _line_number(128)
	, _bRunScript(0)


{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	demo_dlg_ = this;
}

void CXTileScriptingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_GCU_MAC, _mac_str);
	DDX_Control(pDX, IDC_IPADDRESS_GCU_IP, _ip_control);
	DDX_Text(pDX, IDC_EDIT_GCU_CMD_PORT, _cmd_port);
	DDX_Text(pDX, IDC_EDIT_GCU_IMG_PORT, _img_port);
	DDX_Control(pDX, IDC_EDIT_FILENAME, m_Pathname);
	DDX_Control(pDX, IDC_READ_SCRIPT_NAME, m_Filename);
	DDX_Control(pDX, IDC_TEST_RUN_PARAMS, m_OutputFile);
	DDX_Control(pDX, IDC_CONNECTED, m_connected);
	DDX_Text(pDX, IDC_EDIT_TILE_X, _tile_x);
	DDX_Text(pDX, IDC_EDIT_TILE_Y, _tile_y);

}

BEGIN_MESSAGE_MAP(CXTileScriptingDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CONNECT_DETECTORS, &CXTileScriptingDlg::OnBnClickedConnect)
	//ON_EN_SETFOCUS(IDC_READ_SCRIPT, &CXTileScriptingDlg::OnSetfocusEditFilename)
	ON_BN_CLICKED(IDC_READ_SCRIPT_FILE, &CXTileScriptingDlg::OnBnClickedReadScriptFile)
	ON_BN_CLICKED(IDC_RUN_SCRIPT, &CXTileScriptingDlg::OnBnClickedRunScript)
	ON_MESSAGE(WM_FRAME_READY, OnFrameReadyMsg)
	ON_BN_CLICKED(IDC_CONVERT_TEMPS, &CXTileScriptingDlg::OnBnClickedConvertTemps)
END_MESSAGE_MAP()


DWORD ConvertIP(DWORD IP)
{
	DWORD temp;
	temp = (BYTE)IP;
	temp <<= 8;
	temp |= (BYTE)(IP >> 8);
	temp <<= 8;
	temp |= (BYTE)(IP >> 16);
	temp <<= 8;
	temp |= (BYTE)(IP >> 24);
	return temp;
}

CString GetMacString(uint8_t* mac_)
{
	char mac_str[20];
	for (int8_t i = 0; i < 6; i++)
		sprintf(&mac_str[2 * i], "%02X", mac_[i]);
	mac_str[12] = 0;
	CString str(mac_str);
	return str;
}

// CXTileScriptingDlg message handlers

BOOL CXTileScriptingDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	GetDlgItem(IDC_READ_SCRIPT_NAME)->EnableWindow(0);


	char pBuf[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, (LPSTR)pBuf);

	m_Pathname.SetWindowText((LPCTSTR)pBuf);
	m_connected.SetWindowText((LPCTSTR)"Not Connected");
	GetDlgItem(IDC_CONNECTED)->EnableWindow(0);

	_cur_dev = 0;
	RegisterSink();

	UpdateData(1);
	WSADATA wsData;
	::WSAStartup(MAKEWORD(2, 2), &wsData);
	char host_name[128];

	if (gethostname(host_name, 128) == 0)
	{
		// Get host adresses
		struct hostent* host_;
		int i;
		_dev_num = 0;
		host_ = gethostbyname(host_name);
		for (i = 0; host_ != NULL && host_->h_addr_list[i] != NULL; i++)
		{

			LPCSTR psz = inet_ntoa(*(struct in_addr*)host_->h_addr_list[i]);
			//_local_ip_combo.AddString(psz);
			this->_xsystem.SetLocalIP(psz);
			if (this->_xsystem.Open())
			{
				_dev_num = this->_xsystem.FindDevice();
				if (_dev_num > 0)
					break;
				else
					this->_xsystem.Close();
			}
		}
		if (_dev_num > 0)
		{
			if (_dev_num > DEVICE_NUM)
				_dev_num = DEVICE_NUM;
			this->_dev_num = _dev_num;
			for (i = 0; i < _dev_num; i++)
			{
				XDevice* dev_ = this->_xsystem.GetDevice(i);
				uint8_t* mac_ = dev_->GetMAC();
				_mac_str = GetMacString(mac_);
				DWORD ip_value = ConvertIP(inet_addr(dev_->GetIP()));
				_ip_control.SetAddress(ip_value);
				_cmd_port = dev_->GetCmdPort();
				_img_port = dev_->GetImgPort();
				GetDlgItem(IDC_EDIT_GCU_CMD_PORT)->EnableWindow(0);
				GetDlgItem(IDC_EDIT_GCU_IMG_PORT)->EnableWindow(0);
				GetDlgItem(IDC_IPADDRESS_GCU_IP)->EnableWindow(0);
				GetDlgItem(IDC_GCU_MAC)->EnableWindow(0);
			}
		}
		else
		{
			AfxMessageBox(_T("No device found"));
		}
	}
	else {
		AfxMessageBox(_T("gethostbyname failed"));

	}
	UpdateData(0);

	return TRUE;




}

void CXTileScriptingDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CXTileScriptingDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CXTileScriptingDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CXTileScriptingDlg::OnBnClickedConnect()
{
	if (_dev_num > 0)
	{
		_grid_x = 1;
		_grid_y = 1;
		_line_number = _grid_x * _grid_y;
		demo_dlg_->_xarea_correct.SetPara(XAREA_PARA_GRID_WIDTH, _grid_x);
		demo_dlg_->_xarea_correct.SetPara(XAREA_PARA_GRID_HEIGHT, _grid_y);

		//For 10x1 alignment
		demo_dlg_->_xarea_correct.SetPara(XAREA_PARA_TILE_WIDTH, _tile_x);
		demo_dlg_->_xarea_correct.SetPara(XAREA_PARA_TILE_HEIGHT, _tile_y);

		//realloc 
		if (this->_xanalyze) {
			this->_xanalyze->~XAnalyze();
		}
		this->_xanalyze = new XAnalyze();

		for (uint32_t memsize = 2 << 30; memsize > 10240; memsize = memsize >> 5) {

			this->tempbuf = (char*)malloc(memsize);//
			if (this->tempbuf) {
				this->bufsize = memsize;
				break;
			}
			else {
				this->bufsize = 0;
			}
		}
		this->bufuse = 0;
		//CString memstatus;
		//memstatus.Format("Cache Size: %d Mbyte", demo_dlg_->bufsize >> 20);
		//demo_dlg_->_mem_status.SetWindowTextA(memstatus);
		//dev is for GCU , in this case only use one GCU.
		//but this source can handle more GCUs

		//xfactory is the  Resources for every control class
		//for more detail please check XLib usermanual
		this->_xcommand[0].SetFactory(&_xfactory);
		this->_xacquisition[0].SetFactory(&_xfactory);
		this->_xtransfer[0].SetLineNum(_line_number);
		this->_xacquisition[0].RegisterFrameTransfer(&_xtransfer[0]);
		this->_xacquisition[0].EnableLineInfo(0);
		XDevice* xsys = this->_xsystem.GetDevice(0);
		int xcommand_result = this->_xcommand[0].Open(this->_xsystem.GetDevice(0));
		XCommand* xcom = &this->_xcommand[0];
//		bool result = this->_xacquisition[0].Open(this->_xsystem.GetDevice(0), &this->_xcommand[0]);
		bool result = this->_xacquisition[0].Open(xsys, &this->_xcommand[0]);
		uint32_t last_error = this->_xacquisition[0].GetLastError();
		this->_xoff_correct[0].Open(this->_xsystem.GetDevice(0));
		this->_xtransfer[0].RegisterCorrect(&this->_xoff_correct[0]);

		UpdateData(1);

		//get GCU serial number
		this->GetSN();

		uint64_t para_data = 0;

		this->_xcommand[0].GetPara(XPARA_CH_NUM, para_data);

		_card_nums.Format("%d : %d : %d : %d : %d",
			0xff & (para_data >> 32),
			0xff & (para_data >> 24),
			0xff & (para_data >> 16),
			0xff & (para_data >> 8),
			0xff & (para_data));

		uint8_t total_card = 0;
		//here get the number of x-tile on every slot on GCU
		//if it is not equal between _tile_x*_tile_y with total card, 
		//will pop a messagebox
		for (int i = 0; i < _dev_num; i++)
		{
			this->_xcommand[0].GetPara(XPARA_CH_NUM, para_data);
			total_card += static_cast<UCHAR>(para_data);
			total_card += static_cast<UCHAR>(para_data >> 8);
			total_card += static_cast<UCHAR>(para_data >> 16);
			total_card += static_cast<UCHAR>(para_data >> 24);
			total_card += static_cast<UCHAR>(para_data >> 32);
		}
		m_connected.SetWindowText(_T(_card_nums));
		_connected = TRUE;
		UpdateData(0);
	}
}


void CXTileScriptingDlg::OnIpnFieldchangedIpaddressGcuIp(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMIPADDRESS pIPAddr = reinterpret_cast<LPNMIPADDRESS>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


std::string convertToString(char* a, int size)
{
	int i;
	std::string s = "";
	for (i = 0; i < size; i++) {
		s = s + a[i];
	}
	return s;
}

void CXTileScriptingDlg::GetSN()
{
	char sn_str[64];
	std::string sn = "SN: ";
	//get GUN serial number
	if (_xcommand[0].GetPara(XPARA_GCU_SERIAL, sn_str))
	{

		std::string sn2 = convertToString(sn_str, sizeof(sn_str));
		sn2 = sn + sn2;
	}
}

BOOL CXTileScriptingDlg::PreTranslateMessage(MSG* pMsg)
{
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_FILENAME);
	ASSERT(pEdit && pEdit->GetSafeHwnd());

	if (WM_LBUTTONDOWN == pMsg->message && pEdit->GetSafeHwnd() == pMsg->hwnd)
	{
		_bFocusFilename = TRUE;
		BROWSEINFO bi;
		char szPath[MAX_PATH];
		ZeroMemory((void*)&bi, sizeof(bi));
		ZeroMemory(szPath, sizeof(szPath));
		bi.hwndOwner = this->m_hWnd;
		bi.pidlRoot = NULL;
		bi.pszDisplayName = (LPSTR)szPath;
		bi.lpszTitle = (LPCSTR)"Select Save Path";
		bi.ulFlags = BIF_RETURNONLYFSDIRS; ///< select path	
		bi.lpfn = NULL;
		bi.lParam = NULL;
		bi.iImage = NULL;
		LPITEMIDLIST lpdList = SHBrowseForFolder(&bi);
		if (NULL == lpdList) {
			return TRUE; ///< select path	
		}
		else if (SHGetPathFromIDList(lpdList, (LPSTR)szPath))
		{		// szPath is valid;		
			this->m_Pathname.SetWindowText((LPCTSTR)szPath);
		}
		return TRUE;                // Do not process further
	}
	else if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;                // Do not process further
		}
	}
	return CWnd::PreTranslateMessage(pMsg);
}

BOOL CXTileScriptingDlg::ParseJsonFile()
{
	JSONCPP_STRING err;
	Json::Value root;

	std::ifstream myFile(_ScriptFile);
	std::ostringstream tmp;
	tmp << myFile.rdbuf();
	std::string rawJson = tmp.str();

	auto rawJsonLength = static_cast<int>(rawJson.length());
	//std::cout << "rawJsonLength = " << rawJsonLength << std::endl;
	//std::cout << rawJson << std::endl;

	Json::CharReaderBuilder builder;
	const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
	if (!reader->parse(rawJson.c_str(), rawJson.c_str() + rawJsonLength, &root,
		&err)) {
		//std::cout << "error" << std::endl;
		return FALSE;
	}

	// clear script settings
	for (int i = 0; i < MAX_NUM_SCRIPT_RUNS; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			_script_params[i][j] = 0;
		}
		for (int k = 0; k < 8; k++)
		{
			_script_gains[i][k] = -1;
		}
	}

	Json::Value data;
	_num_gain_settings = 0;
	const Json::Value test_runs = root["test_run"];
	if (test_runs.size() == 0)
		return FALSE;
	for (int i = 0; i < test_runs.size(); ++i)
	{
		_script_params[i][KV_SETTING] = test_runs[i]["kV"].asInt();
		_script_params[i][MA_SETTING] = test_runs[i]["mA"].asInt();
		_script_params[i][FRAMES] = test_runs[i]["Frames"].asInt();
		_script_params[i][BITS] = test_runs[i]["Bits"].asInt();
		_script_params[i][INT_TIME] = test_runs[i]["IntTime"].asInt();
		_script_params[i][DMS] = test_runs[i]["DM"].asInt();
		_num_gain_settings = 0;
		for (int j = 0; j < test_runs[i]["GainSettings"].size(); j++)
		{
			_script_gains[i][j] = ConvertGainString(&test_runs[i]["GainSettings"][j]["Gain"].asString());
			_num_gain_settings += 1;
		}
	}
	SetOutputFileName(0);
	return TRUE;
}

void CXTileScriptingDlg::OnBnClickedReadScriptFile()
{
	const TCHAR szFilter[] = _T("JSON Files (*.json)|*.json|All Files (*.*)|*.*||");
	CFileDialog dlg(TRUE, _T("json"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, this);
	if (dlg.DoModal() == IDOK)
	{
		_ScriptFile = dlg.GetPathName();
		if (this->ParseJsonFile())
		{
			m_Filename.SetWindowText(_ScriptFile);
			std::string s = _kV + "kV_" + _mA + "mA_" + _bits + ":bitRange_" + _frames + "_frames_" + _intTime + "uSec_";
			_outputFileName = CString(s.c_str());
			m_OutputFile.SetWindowText(_outputFileName);
		}
		else
		{
			_ScriptFile = '\0';
			AfxMessageBox(_T("Script File Parse Error."));
			m_Filename.SetWindowText(_T("Script File Parse Error"));
		}
		UpdateData(1);
	}
}

LRESULT	CXTileScriptingDlg::OnFrameReadyMsg(WPARAM wParam, LPARAM lParam)
{
	_frame_counter++;

	if (demo_dlg_->_total_frames && _frame_counter == demo_dlg_->_total_frames )
	{
		//stop acquisition
		demo_dlg_->_xacquisition[0].Stop();

		demo_dlg_->m_Pathname.GetWindowTextA(demo_dlg_->_SavePrefix);
		CString gain_str = demo_dlg_->ConvertGainInt(_script_gains[_cur_test_run][_cur_gain]);
		CString filename;
		filename.Format("%s\\%s_%s%s",
			demo_dlg_->_SavePrefix,
			_outputFileName,
			gain_str,
			".raw");

		FILE* _cache_file = fopen(filename.GetBuffer(), "wb");
		if (_cache_file && demo_dlg_->bufuse) {

			fwrite(demo_dlg_->tempbuf, demo_dlg_->bufuse, 1, _cache_file);
			fclose(_cache_file);
			_cache_file = 0;
		}
		Sleep(500);
		WriteAuxFile(&gain_str);
		demo_dlg_->_bFileSaved = 1;

		demo_dlg_->OnBnClickedRunScript();
	}
	return 0;
}

int CXTileScriptingDlg::ConvertGainString(std::string* gain)
{
	int gain_index = -1;

	if (gain->compare("6.25pC") == 0)
		gain_index = 0x4 << 8 | 0x4;
	else if (gain->compare("12.5pC") == 0)
		gain_index = 0x3 << 8 | 0x3;
	else if (gain->compare("25pC") == 0)
		gain_index = 0x2 << 8 | 0x2;
	else if (gain->compare("37.5pC") == 0)
		gain_index = 0x7 << 8 | 0x7;
	else if (gain->compare("50pC") == 0)
		gain_index = 0x1 << 8 | 0x1;
	else if (gain->compare("75pC") == 0)
		gain_index = 0x6 << 8 | 0x6;
	else if (gain->compare("100pC") == 0)
		gain_index = 0;
	else if (gain->compare("150pC") == 0)
		gain_index = 0x5 << 8 | 0x5;

	return gain_index;
}

CString CXTileScriptingDlg::ConvertGainInt(int gain)
{
	CString gain_str = (const char*)"";

	if (gain == 1028)
		gain_str = "6.25pC";
	else if (gain == 771)
		gain_str = "12.5pC";
	else if (gain == 514)
		gain_str = "25pC";
	else if (gain == 1799)
		gain_str = "37.5pC";
	else if (gain == 257)
		gain_str = "50pC";
	else if (gain == 1542)
		gain_str = "75pC";
	else if (gain == 0)
		gain_str = "100pC";
	else if (gain == 1285)
		gain_str = "150pC";

	return gain_str;
}


void CXTileScriptingDlg::SetOutputFileName(int test_run_index)
{
	// generate outputFileName for first script test run
	_kV = std::to_string(_script_params[test_run_index][0]);
	_mA = std::to_string(_script_params[test_run_index][1]);
	_frames = std::to_string(_script_params[test_run_index][2]);
	_bits = std::to_string(_script_params[test_run_index][3]);
	_intTime = std::to_string(_script_params[test_run_index][4]);
	std::string s = _kV + "kV_" + _mA + "mA_" + _bits + "_bitRange_" + _frames + "_frames_" + _intTime + "uSec_";
	_outputFileName = CString(s.c_str());
	m_OutputFile.SetWindowText(_outputFileName);
}



CWnd* CXTileScriptingDlg::GetDispWnd()
{
	CWnd* dialog = GetDlgItem(IDC_DISPLAY_WND);
	return dialog;
}

void CXTileScriptingDlg::OnBnClickedBtnCap( int Start)
{
	char recv_str[100] = "";

	if (Start)
	{
		//offset correction
		int result = _xcommand->SendAscCmd("[OC,E,0]", recv_str);
		Sleep(500);

		demo_dlg_->_bDisplay = 0;
		demo_dlg_->_bSaveImage = 1;

		// set integration time
		UINT int_time = _script_params[_cur_test_run][INT_TIME];
		if (int_time < 100) _int_time = 100;
		if (int_time > 100000) _int_time = 100000;
		_xcommand->SetPara(XPARA_INT_TIME, int_time);

		//set frames
		this->_total_frames = _script_params[_cur_test_run][FRAMES];
		//set gain
		_xcommand->SetPara(XPARA_DM_GAIN, _script_gains[_cur_test_run][_cur_gain], 0xFF);

		//Something about save file
		//sync ._SaveInterval  shift number
//		demo_dlg_->_SaveInterval = demo_dlg_->_SaveInterval ?
//			(2 << (demo_dlg_->_SaveInterval - 1)) : (1);
		//new grab and dont' save yet
		demo_dlg_->_SaveInterval = 1;
		demo_dlg_->_bFileSaved = 0;

		//cache memory for save file
		demo_dlg_->bufuse = 0;
		//clear buffer
		memset(demo_dlg_->tempbuf, 0x00, demo_dlg_->bufsize);
		//file path
		demo_dlg_->m_Filename.GetWindowTextA(demo_dlg_->_SavePrefix);
		demo_dlg_->_SaveCount = 0;//Saving count number
		demo_dlg_->_SaveFlag = demo_dlg_->_SaveInterval;//
		demo_dlg_->_frame_counter = 0;
		demo_dlg_->_line_lost_num = 0;
		//demo_dlg_->OnEventMsg(53, 0);
		//demo_dlg_->OnEventMsg(90, 0);

		int res_xac = demo_dlg_->_xacquisition[0].Grab(_script_params[_cur_test_run][FRAMES]);
	}
	else
	{
		//stop grab
		int i = 0;
		demo_dlg_->_xacquisition[0].Stop();
/*
		int _is_raw = demo_dlg_->_SaveSuffix.Find("raw");
		if (demo_dlg_->_bSaveImage
			&& !demo_dlg_->_bFileSaved
			&& _is_raw != -1)
		{
			CString gain_str = ConvertGainInt(_script_gains[_cur_test_run][_cur_gain]);
			CString filename;
			filename.Format("%s\\%s_%s%s",
				demo_dlg_->_SavePrefix,
				_outputFileName,
				gain_str,
				demo_dlg_->_SaveSuffix);

			FILE* _cache_file = fopen((const char*)filename.GetBuffer(), "wb");
			if (_cache_file && demo_dlg_->bufuse) {

				fwrite(demo_dlg_->tempbuf, demo_dlg_->bufuse, 1, _cache_file);
				fclose(_cache_file);
				_cache_file = 0;
			}
			demo_dlg_->_bFileSaved = 1;
		}
		demo_dlg_->OnEventMsg(91, 0);
*/
	}
	UpdateData(false);
}


void CXTileScriptingDlg::OnBnClickedRunScript()
{
	char recv_str[100] = "";
	int new_gain = -1;
	int new_test_run = 0;

	if (!_connected)
	{
		AfxMessageBox(_T("Detectors not connected. Can't run script."));
	}
	if (_ScriptFile[0] != '\0')
	{
		if (!demo_dlg_->_bRunScript)
		{
			//first time thru
			demo_dlg_->_bRunScript = 1;
			CString test_run_msg;
			SetOutputFileName(_cur_test_run);
			test_run_msg.Format("Test Run Set kV = %d and mA = %d", _script_params[_cur_test_run][KV_SETTING],
				_script_params[_cur_test_run][MA_SETTING]);
			AfxMessageBox(test_run_msg);
		}
		else
		{
			AfxMessageBox(_T("Continue?"));

			if (_cur_gain < NUM_GAIN_SETTINGS - 1)
			{
				new_gain = _script_gains[_cur_test_run][_cur_gain + 1];
				if (new_gain != -1)
				{
					_cur_gain += 1;
				}
			}
			if ((_cur_gain == NUM_GAIN_SETTINGS) || (new_gain == -1))
			{
				// check if we have another test run
				if (_script_params[_cur_test_run + 1][KV_SETTING] != 0)
				{
					_cur_gain = 0;
					_cur_test_run += 1;
					CString test_run_msg;
					test_run_msg.Format("Test Run Set kV = %d and mA = %d", _script_params[_cur_test_run][KV_SETTING],
						_script_params[_cur_test_run][MA_SETTING]);
					AfxMessageBox(test_run_msg);
					SetOutputFileName(_cur_test_run);
				}
				else
				{
					_cur_gain = 0;
					_cur_test_run = 0;
					// done running script
					demo_dlg_->_bRunScript = 0;
					AfxMessageBox(_T("Script Complete"));
				}
			}
		}
		if (demo_dlg_->_bRunScript)
		{
			this->bufuse = 0;
			OnBnClickedBtnCap(1);
		}
		UpdateData(0);
	}
	else
	{
		AfxMessageBox(_T("No script file read."));
	}
}



uint32_t CXTileScriptingDlg::hex2int(char* hex) {
	uint32_t val = 0;

	for (int i = 0; i < 4; i++)
	{
		// get current character then increment
		uint8_t byte = *hex++;
		// transform buf character to the 4bit equivalent number, using the ascii table indexes
		if (byte >= '0' && byte <= '9') byte = byte - '0';
		else if (byte >= 'a' && byte <= 'f') byte = byte - 'a' + 10;
		else if (byte >= 'A' && byte <= 'F') byte = byte - 'A' + 10;
		// shift 4 to make space for new digit, and add the 4 bits of the new digit 
		val = (val << 4) | (byte & 0xF);
	}
	return val;
}

void CXTileScriptingDlg::WriteAuxFile(CString* gain)
{
	uint64_t para_data = 0;
	char gain_str[20];
	char temp_str[256] = "";
	char convert_str[4];
	int int_time;
	std::string output = "";
	CString filename = "";

	for (int i = 0; i < 256; i++)
		temp_str[i] = 0;

	//get intergation time and show in dialog
	this->_xcommand[0].GetPara(XPARA_INT_TIME, para_data);
	int_time = static_cast<UINT>(para_data);
	//get current gain
	this->_xcommand[0].SendAscCmd("[SG,R,1]", gain_str);
	//get voltage and temperature string
	this->_xcommand[0].SendAscCmd("[DI,R,FF]", temp_str);


	output = "Int_Time: " + std::to_string(int_time) + " Gain: " + gain_str + " VoltageTemps: " + temp_str + '\n';
	for (int i = 0; i < 4; i++)
	{
		int temp = hex2int(&temp_str[35] + (i * 44));
		float temp1 = temp * .125;
		temp = hex2int(&temp_str[39] + (i * 44));
		float temp2 = temp * .002472;
		temp = hex2int(&temp_str[43] + (i * 44));
		float temp3 = temp * .002472;
		output += "PCB_Temp = " + std::to_string(temp1) + " SideA_Temp = " + std::to_string(temp2) + " SideB_Temp = " + std::to_string(temp3) + '\n';
	}
	demo_dlg_->m_Pathname.GetWindowTextA(demo_dlg_->_SavePrefix);
	filename.Format("%s\\%s_%s%s", demo_dlg_->_SavePrefix,_outputFileName, *gain, ".aux");

	FILE* aux_file = fopen(filename.GetBuffer(), "wb");
	if (aux_file) {

		fwrite(output.c_str(), output.size(), 1, aux_file);
		fclose(aux_file);
	}

}

LRESULT CXTileScriptingDlg::OnErrorMsg(WPARAM wParam, LPARAM lParam)
{

	return 0;
}

LRESULT CXTileScriptingDlg::OnEventMsg(WPARAM wParam, LPARAM lParam)
{
	return 0;
}




float temperature;
float humidity;
CString err_str;

class XCmdSink : public IXCmdSink
{
public:
	void OnXError(uint32_t err_id, const char* err_msg_)
	{
		err_str = "Error: " + CString(err_msg_);
		//PostMessage(this->m_hWnd, WM_ERROR, 0, 0);
	}
	void OnXEvent(uint32_t event_id, float data)
	{
		switch (event_id)
		{
		case 56:
			temperature = data;
			break;
		case 57:
			humidity = data;
			break;
		}
		//PostMessage(demo_dlg_->m_hWnd, WM_EVENT, event_id, 0);
	}
};

class XImgSink : public IXImgSink
{
public:
	int _line_lost;
	int _frame_lines;//  = tile_w  * tile_h * gird_w * gird_h 

	void OnXError(uint32_t err_id, const char* err_msg_)
	{
		err_str = "Error: " + CString(err_msg_);
		//PostMessage(demo_dlg_->m_hWnd, WM_ERROR, 0, 0);
	}
	void OnXEvent(uint32_t event_id, uint32_t data)
	{
		//PostMessage(demo_dlg_->m_hWnd, WM_EVENT, event_id, data);
		if (event_id == 53)
			_line_lost += data;
	}
	void OnFrameReady(XImage* image_)
	{
		demo_dlg_->_cur_image_ = 0;
		if (image_ == 0) return;

		demo_dlg_->_xarea_correct.DoCorrect(image_);
		demo_dlg_->_cur_image_ = demo_dlg_->_xarea_correct.GetAreaImage();

		PostMessage(demo_dlg_->m_hWnd,
			WM_FRAME_READY,
			(WPARAM)demo_dlg_->_cur_image_,
			0);

		//both method 
		//click check bSave
		//if no set SavePath will save at default path
		//interval must > 0 ,
		//				0-dont save,
		//				1-means every frames save,
		//				2-means interval one frame
		//check save status
		if (demo_dlg_->_cur_image_ == 0)return;

		int bufleft = demo_dlg_->bufsize - demo_dlg_->bufuse - (uint32_t)demo_dlg_->_cur_image_->_size;
		if (demo_dlg_->_bDisplay == 0                //1, no display
			&& demo_dlg_->_bSaveImage               //2, save Image to raw
			&& demo_dlg_->_SaveInterval > 0
			&& --demo_dlg_->_SaveFlag == 0
			//check cache status
			&& demo_dlg_->tempbuf
			&& demo_dlg_->bufsize > 0
			&& bufleft > 0)
		{

			//copy data line by line 
			uint32_t row;
			uint32_t pixel_byte = 2;
			if (demo_dlg_->_cur_image_->_pixel_depth > 16)
				pixel_byte = 4;
			for (row = 0; row < demo_dlg_->_cur_image_->_height; row++)
			{
				uint8_t* row_dat = demo_dlg_->_cur_image_->GetLineAddr(row) + demo_dlg_->_cur_image_->_data_offset;
				memcpy(demo_dlg_->tempbuf + demo_dlg_->bufuse,
					row_dat,
					demo_dlg_->_cur_image_->_width * pixel_byte);
				demo_dlg_->bufuse += demo_dlg_->_cur_image_->_width * pixel_byte;
			}

			//memory cache for saving file support interval , so reset it.
			demo_dlg_->_SaveFlag = demo_dlg_->_SaveInterval;

			if ((demo_dlg_->bufsize - demo_dlg_->bufuse)
				< demo_dlg_->_cur_image_->_size)
			{
				//cache is full , stop
				demo_dlg_->OnBnClickedBtnCap(0);//.OnBnClickedGrab();
			}
			else {}
		}
		// save file on other conditions
		else if (demo_dlg_->_bSaveImage
			&& demo_dlg_->_SaveInterval > 0
			&& --demo_dlg_->_SaveFlag == 0) {
			//save a frame
			demo_dlg_->_bFileSaved = 1;
			CString filename;
			filename.Format("%s\\image%d%s",
				demo_dlg_->_SavePrefix,
				demo_dlg_->_SaveCount,
				demo_dlg_->_SaveSuffix);
			demo_dlg_->SaveImage(filename);//save file
			demo_dlg_->_SaveCount++;
			demo_dlg_->_SaveFlag = demo_dlg_->_SaveInterval;
		}
	}
};


XCmdSink xcmd_sink;
XImgSink ximg_sink;

void CXTileScriptingDlg::RegisterSink()
{
	_xsystem.RegisterEventSink(&xcmd_sink);
	for (int i = 0; i < DEVICE_NUM; i++)
	{
		_xcommand[i].RegisterEventSink(&xcmd_sink);
		_xacquisition[i].RegisterEventSink(&ximg_sink);
		_xtransfer[i].RegisterEventSink(&ximg_sink);
	}
	_xmulti_transfer.RegisterEventSink(&ximg_sink);
}

void CXTileScriptingDlg::SaveImage(CString file_name)
{
	//XLib is only support save raw, txt.
	//
	if (strstr(file_name.GetBuffer(), ".tif"))
	{
		//get the first Device.
		XTifFormat tif_file(demo_dlg_->_cur_image_, demo_dlg_->_xsystem.GetDevice(0));
		//save tif file
		tif_file.Save(file_name);
	}
	else if (_cur_image_)
	{
		_cur_image_->Save(file_name.GetBuffer());
	}
	else {}
}

void CXTileScriptingDlg::OnClose()
{
	_xdisplay.Close();

	for (int i = 0; i < DEVICE_NUM; i++)
	{
		_xacquisition[i].Close();
		_xcommand[i].Close();
		_xoff_correct[i].Close();
	}
	_xmulti_transfer.Close();
	_xsystem.Close();
	CDialog::OnClose();
}

void CXTileScriptingDlg::OnBnClickedConvertTemps()
{
	fstream newfile;
	ofstream outfile;
	CString filename_in;
	CString filename_out;
	demo_dlg_->m_Pathname.GetWindowTextA(demo_dlg_->_SavePrefix);
	filename_in.Format("%s\\%s", demo_dlg_->_SavePrefix,"XTile_temp_data.txt");
	filename_out.Format("%s\\%s", demo_dlg_->_SavePrefix, "XTile_temp_data_converted.txt");

	//FILE* _temps_file = fopen(filename.GetBuffer(), "r");

	newfile.open(filename_in, ios::in);
	outfile.open(filename_out, ios::out);
	if (newfile.is_open() && outfile.is_open()) { //checking whether the file is open
		string tp;
		string output;
		int line_count = 0;
		while (getline(newfile, tp)) 
		{ //read data from file object and put it into string.
			outfile << tp << "\n"; 
			line_count++;
			if ((line_count % 2) == 0)
			{
				for (int i = 0; i < 4; i++)
				{
					int temp = hex2int(&tp[32] + (i * 44));
					float temp1 = temp * .125;
					temp = hex2int(&tp[36] + (i * 44));
					float temp2 = temp * .002472;
					temp = hex2int(&tp[40] + (i * 44));
					float temp3 = temp * .002472;
					output = "X-Tile[" + to_string(i + 1) + "] PCB_Temp = " + to_string(temp1) + " SideA_Temp = " + to_string(temp2) + " SideB_Temp = " + to_string(temp3) + '\n';
					outfile << output << "\n";
				}

			}
		}
		newfile.close(); //close the file object.
		outfile.close(); //close the file object.
	}
}
